import * as THREE from 'three';

type IMesh = {
    geometry: THREE.BufferGeometry,
    color: string,
    scale: number
} 

export const meshes: IMesh[] = [{
    geometry: new THREE.BoxGeometry(1, 1, 1),
    color: '#fff',
    scale: 0.5
},{
    geometry: new THREE.RingGeometry(0.5, 1, 20, 2),
    color: 'red',
    scale: 0.4
},{
    geometry: new THREE.CylinderGeometry(0.5, 1, 2, 32, 4, true),
    color: 'blue',
    scale: 0.3
},{
    geometry: new THREE.ConeGeometry(1, 2, 20, 2, true, ),
    color: 'aquamarine',
    scale: 0.3
},{
    geometry: new THREE.IcosahedronGeometry(1, 0),
    color: 'green',
    scale: 0.4
},{
    geometry: new THREE.TorusKnotGeometry(1, 0.25, 100, 10),
    color: 'aqua',
    scale: 0.25
},{
    geometry: new THREE.TetrahedronGeometry(1, 0),
    color: 'brown',
    scale: 0.4
},{
    geometry: new THREE.TorusGeometry(1, 0.5, 10, 32),
    color: 'orange',
    scale: 0.25
},{
    geometry: new THREE.SphereGeometry(1, 32, 16, 0),
    color: 'yellow',
    scale: 0.4
},]