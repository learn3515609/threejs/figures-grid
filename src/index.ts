import * as THREE from 'three';
import TWEEN from 'three/examples/jsm/libs/tween.module';
import { meshes } from './meshes';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import './style.css';
import Stats from 'stats.js';
import * as dat from 'lil-gui'

const canvas = document.getElementsByTagName('canvas')[0];
const renderer = new THREE.WebGLRenderer({canvas: canvas});
const raycaster = new THREE.Raycaster();

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight);
camera.position.z = 4;

const controls = new OrbitControls(camera, canvas);
const stats = new Stats();
stats.showPanel(0);
document.body.appendChild(stats.dom);

const gui = new dat.GUI();

let activeMeshIndex = -1;

const group = new THREE.Group();
let k = 0;
for(let y = -1; y <= 1; y++) {
    for(let x = -1; x <= 1; x++, k++) {
        const m = meshes.shift();
        const material = new THREE.MeshBasicMaterial({ color: 'gray', wireframe: true })
        const mesh = new THREE.Mesh(m.geometry, material);
        mesh.position.set(x, -y, 1);
        mesh.scale.set(m.scale, m.scale, m.scale);
        Object.assign(mesh, {color: m.color});
        Object.assign(mesh, {index: k});
        Object.assign(mesh, {basePosition: new THREE.Vector3(x, -y, 1)});
        const folder = gui.addFolder(`Mesh ${k}`).close()
        folder.add(mesh.scale, 'x').min(0).max(5).step(0.1).name(`${k}: x`);
        folder.add(mesh, 'visible').name(`${k}: visible`);
        folder.add(mesh.material, 'wireframe').name(`${k}: wireframe`);
        group.add(mesh);
    }
}
scene.add(group);

renderer.setSize(window.innerWidth, window.innerHeight);

const clearPrevMesh = () => {
    const prevMesh = (group.children[activeMeshIndex] as any);
    prevMesh.material.color.set('gray');
    new TWEEN.Tween(prevMesh.position).to(prevMesh.basePosition, Math.random() * 1000 + 1000)
        .easing(TWEEN.Easing.Exponential.Out)
        .start();
    activeMeshIndex = -1;
}


const clock = new THREE.Clock();
const tick = () => {
    stats.begin();
    if(activeMeshIndex !== -1) {
        const delta = clock.getDelta();
        group.children[activeMeshIndex].rotation.y += delta;
    }

    controls.update();
    TWEEN.update();
    renderer.render(scene, camera);
    stats.end();
    window.requestAnimationFrame(tick);
}

const onResize = () => {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.render(scene, camera);
}

const onClick = (event: MouseEvent) => {
    const pointer  = new THREE.Vector2();
    pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
    pointer.y = -(event.clientY / window.innerHeight) * 2 + 1;
    raycaster.setFromCamera(pointer, camera);
    activeMeshIndex !== -1 && clearPrevMesh();
    const intersections = raycaster.intersectObjects(group.children);
    intersections.forEach(item => {
        const object = item.object as THREE.Mesh;
        const color: string = (object as any).color;
        (object.material as any).color.set(color);
        activeMeshIndex = (object as any).index;
        
        new TWEEN.Tween(object.position).to({
            x: 0,
            y: 0,
            z: 3
        }, Math.random() * 1000 + 1000)
            .easing(TWEEN.Easing.Exponential.Out)
            .start();
    })
}

window.addEventListener('resize', onResize)
window.addEventListener('click', onClick)
tick();